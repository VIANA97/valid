Então('sou redirecionado para dentro do meu contexto:') do |contexto|
    produto = contexto.hashes.first
    expect(page).to have_selector('h1', text: produto[:contexto])
  end