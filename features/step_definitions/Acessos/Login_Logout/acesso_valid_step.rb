Dado('que acesso a página de login') do
   visit '/'
end

Quando('submeto o seguinte acesso {string} e {string}') do |email, senha|
    @login_valid.with(email, senha)
end

E('clico no botão entrar') do
    @login_valid.botao_entrar
end

Quando('submeto o seguinte formulário de acesso:') do |table|
    user = table.hashes.first
    find("#txt_login").set user[:email]
    find("#txt_password").set user[:senha]
end

Então('sou redirecionado para o Dashboard') do
    expect(page).to have_css ".breadcrumb"
end

Então('o botão de entrar desabilita') do
    expect(page).to have_button('Entrar', disabled: true)
end

Então('vejo a mensagem de alerta: {string}') do |mensagem| 
    expect(page).to have_content mensagem
end