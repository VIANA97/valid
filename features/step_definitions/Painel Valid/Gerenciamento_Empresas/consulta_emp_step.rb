Dado('submeto o seguinte acesso VALID {string} e {string}') do |email, senha|
    @login_valid.with(email, senha)
    @login_valid.botao_entrar
    
    if has_css?('ul[class="nav-secondary"]')
        find('i[class="fas fa-chart-pie"]').click
    end
end
   
Quando('clico no opção Gerenciamento de Empresas') do
    @ger_empresa.botao_ger_empresa
end
  
Então('sou redirecionado a página Gerenciar Empresa') do
    expect(page).to have_css ".title-xl"
end

Dado('estou na tela de Gerenciar Empresa') do
    @ger_empresa.botao_ger_empresa
end
  
Quando('submeto a seguinte pesquisa valida:') do |validos|
    @empresa = validos.hashes.first    
    @consulta_emp.campo_nome.set @empresa[:nome]
    @consulta_emp.campo_email.set @empresa[:email]
    @consulta_emp.botao_pesquisar
end
  
Então('sistema retornará empresa dos campos pesquisado') do
    mensagem = find('tbody tr').text
    expect(mensagem).to have_content @empresa[:nome]
    expect(mensagem).to have_content @empresa[:email]
end


Quando('submeto a seguinte pesquisa invalida:') do |invalidos|
    @empresa = invalidos.hashes.first    
    @consulta_emp.campo_nome.set @empresa[:nome]
    @consulta_emp.campo_email.set @empresa[:email]
    @consulta_emp.botao_pesquisar
end
  
Então('vejo mensagem de alerta: {string}') do |mensagem|
    alerta = find('div[class="warning"]').text
    expect(alerta).to have_content mensagem
end