Dado('realizo a pesquisa da empresa {string}') do |nome|
    @nome = nome
    @consulta_emp.campo_nome.set @nome
    @consulta_emp.botao_pesquisar
end
  
Quando('seleciono uma empresa para edicao') do
    find('tbody tr', text: @nome).click
end
  
Quando('submeto a alteracao dos campos:') do |edition|
    edicao = edition.hashes.first

    @edit_empresa.edicao_emp(edicao)
end

Quando('submeto a alteracao dos campos invalidando-os:') do |edition_inv1|
    edicao_inv1 = edition_inv1.hashes.first

    @edit_empresa.edicao_inv1_emp(edicao_inv1)
end
  
Então('botão salvar desabilita') do
    expect(page).to have_button('Salvar', disabled: true)
end

Quando('clico no botão salvar') do
    @edit_empresa.botao_salvar
end