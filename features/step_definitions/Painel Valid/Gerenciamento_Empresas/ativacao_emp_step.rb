Dado('realizo a pesquisa da empresa:') do |emp_desativa|
    nome_emp = emp_desativa.hashes.first

    @consulta_emp.campo_nome.set nome_emp[:empresa]
    @consulta_emp.botao_pesquisar
end
  
Quando('inativo o status da empresa') do
    find('label[for="rdb-inactive_id"]').click
end

Quando('ativo o status da empresa') do
    find('label[for="rdb-active_id"]').click
end
