Quando('clico no botão cadastrar nova empresa') do
    @cad_empresa.botao_cadastrar
end
  
Quando('submeto o preenchimento do primeiro passo:') do |passo1|
    cadastro = passo1.hashes.first
    @cad_empresa.cad_empresa(cadastro)
    @cad_empresa.botao_proximo
end
  
Quando('submeto o preenchimento do segundo passo:') do |passo2|
    produto = passo2.hashes.first
    @cad_empresa.escolhe_produto(produto) 
    @cad_empresa.botao_proximo
end
  
Quando('submeto o preenchimento do terceiro passo:') do |passo3|
    estilo = passo3.hashes.first

    @cad_empresa.customizacao(estilo)
    @cad_empresa.botao_finalizar
end


Quando('submeto o preenchimento inválido do primeiro passo:') do |passo1|
    cadastro = passo1.hashes.first
    @cad_empresa.cad_empresa(cadastro)
end

Então('botão próximo desabilita') do
    expect(page).to have_button('Próximo', disabled: true)
end

Quando('seleciono um produto do segundo passo') do
    find('i[class="fa-2x fa-mail-bulk fas"]').click
    @cad_empresa.botao_proximo
end