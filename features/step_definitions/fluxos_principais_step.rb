Dado('seleciono a linguagem para português') do
    @pre_condicao.seleciono_ling
end
  
Dado('clico no para fechar a mensagem de sucesso') do
    @pre_condicao.fechar_mensagem
    sleep 1
end
Dado('acesso a empresa {string}') do |empresa|
    @pre_condicao.acesso_emp(empresa)
end

Dado('acesso a tela de {string}') do |api|
    @pre_condicao.acesso_api(api)
end

Dado('clico para realizar a configuração da api:') do |config_api|
    config_api = config_api.hashes.first
    @pre_condicao.config_API(config_api)
end
  
Dado('clico na {string}') do |f_faturamentos|
    @pre_condicao.acesso_ffaturamentos(f_faturamentos)
end

Dado('acesso a tela de cadastro da tarifa {string}') do |pix|
    @pre_condicao.acesso_cadPIX(pix)
end
  
Dado('clico para cadastrar tarifa pix') do
    @pre_condicao.cad_faixa_blux
end
  
Dado('clico para cadastrar tarifa boleto') do
    @pre_condicao.cad_faixa_blux
end


#Delivery Center --------------------------------------------------------------------------------------------------------------------------------------------------------------

Dado('clico na tela de {string}') do |tela|
    click_button tela
end

Dado('acesso a empresa cadastrada no contexto {string}') do |produto|
    @pre_condicao.produto_contexto(produto)
end

Dado('acesso a tela de Gerenciar Perfil') do
    find('i[class="fas fa-users"]').click
end

Dado('clico para cadastrar um perfil dentro do produto dlv') do
    @pre_condicao.cad_perfil_dlv
end

Dado('acesso a tela de Gerenciar Marcas') do
    sleep 1
    find('i[class="far fa-flag"]').click
end

Dado('clico para cadastrar uma marca dentro do produto dlv') do
    @pre_condicao.cad_marca_dlv
end

Dado('acesso a tela de Gerenciar Usuários') do
    sleep 1
    find('i[class="far fa-user"]').click
end

Dado('clico para cadastrar um usuário Admin dentro do produto dlv') do
    @pre_condicao.cad_user_admin
end

Dado('clico para cadastrar um usuário Comum dentro do produto dlv') do
    @pre_condicao.cad_user_comum
end

Dado('acesso a tela de Configurar Canais') do
    sleep 1
    find('i[class="fas fa-satellite-dish"]').click
end

Dado('clico para configurar canal de {string}') do |numero|
    @pre_condicao.config_canal_num(numero)
end

Dado('clico para configurar canal de E-mail') do
   @pre_condicao.config_canal_email
end

Dado('retorno para o painel VALID') do
    sleep 2
    @pre_condicao.botao_inicio
    sleep 1
    find('span', text: 'Acessar Painel Valid').click
    sleep 1
end
  
Dado('clico para ativar canal de {string}') do |canal_num|
    canal = find('div[class="mat-tab-labels"]')
    canal.first("div", :text => canal_num).click
    @pre_condicao.ativando_canal_num
end

Dado('clico para ativar canal de E-mail') do
    canal = find('div[class="mat-tab-labels"]')
    canal.first("div", :text => "E-mail").click
   @pre_condicao.ativando_canal_email
end

Dado('clico para salvar') do
    click_button "Salvar"
end

Dado('clico no botão inicio') do
    @pre_condicao.botao_inicio
end
  
Dado('clico para bloquear canal de E-mail') do
    click_button  "Novo cadastro"
    find('label[for="EMAIL_id"]').click
    @pre_condicao.canal_email_bloq
end

Dado('clico para bloquear canal de Impresso') do
    click_button  "Novo cadastro"
    find('label[for="PRINTED_id"]').click
    @pre_condicao.canal_impresso_bloq
end
  
Dado('clico para bloquear canal de {string}') do |canal_num|
    click_button  "Novo cadastro"
    find('span', text: canal_num).click
    @pre_condicao.canal_num_bloq
end


#E-diploma -----------------------------------------------------------------------------------------------------------------------------------------------------------------------
Dado('clico para cadastrar um perfil dentro do produto edi') do
    @pre_condicao.cad_perfil_edi
end

Dado('clico para cadastrar uma marca dentro do produto edi') do
    @pre_condicao.cad_marca_edi
end

Dado('clico para cadastrar um usuário Admin dentro do produto edi') do
    @pre_condicao.cad_user_admin_edi
end

Dado('clico para cadastrar um usuário Comum dentro do produto edi') do
    @pre_condicao.cad_user_comum_edi
end

#Blu-X  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------
Dado('clico para cadastrar um perfil dentro do produto blux') do
    @pre_condicao.cad_perfil_blux
end

Dado('clico para cadastrar uma marca dentro do produto blux') do
    @pre_condicao.cad_marca_blux
end

Dado('clico para cadastrar um usuário Comum dentro do produto blux') do
    @pre_condicao.cad_user_comum_blux
end
