#language: pt

Funcionalidade: Acesso VALID
    Sendo um usuário do tipo VALID
    Quero fazer o login
    Para que eu possa acessar o painel VALID

    Contexto:
        Dado que acesso a página de login

    @login_valid
    Cenario: Fazer o login com sucesso

        Quando submeto o seguinte acesso "dev-valid@modalgr.com.br" e "123456"
            E clico no botão entrar
        Então sou redirecionado para o Dashboard


    @botao_valid
    Esquema do Cenario: Tentativa de Login inválido sem um dos campos preenchidos

        Quando submeto o seguinte formulário de acesso:
            | email         | senha         |
            | <email_input> | <senha_input> |
        Então o botão de entrar desabilita

        Exemplos:
            | email_input                  | senha_input |
            |                              | 123456      |
            | dev-valid@modalgr.com.br     |             |


    @alerta_valid
    Esquema do Cenario: Tentativa de Login inválido com valores incorretos

        Quando submeto o seguinte formulário de acesso:
            | email         | senha         |
            | <email_input> | <senha_input> |
            E clico no botão entrar
        Então vejo a mensagem de alerta: "<mensagem_output>"

        Exemplos:
            | email_input                  | senha_input | mensagem_output                                        |
            | matheus.qateste009@gmail.com | 123456      | Usuário não encontrado                                 |
            | dev-valid@modalgr.com.br     | Teste10     | A senha está incorreta ou não possui um formato válido |
