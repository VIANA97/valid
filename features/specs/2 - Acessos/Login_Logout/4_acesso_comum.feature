#language: pt

Funcionalidade: Acesso Comum
    Sendo um usuário do tipo Comum
    Quero fazer o login
    Para que eu possa acessar o contexto que estou atrelado

    Contexto:
        Dado que acesso a página de login

    @login_comum
    Cenario: Fazer o login com sucesso

        Quando submeto o seguinte formulário de acesso:
            | email         | senha         | 
            | <email_input> | <senha_input> |
        E clico no botão entrar
        Então sou redirecionado para dentro do meu contexto:
            | contexto         | 
            | <contexto_input> |

        Exemplos:
        #usuários foram cadastrados dentro da empresa Nestlé
            | email_input                  | senha_input | contexto_input |
            | rogogel423@vysolar.com       | Comum190@   | Delivery Center|
            | sikig56296@neragez.com       | Comum190@   | E-diploma      |
            | sobawi7447@u461.com          | Comum190@   | Blu X          |


    @botao_comum
    Esquema do Cenario: Tentativa de Login inválido sem um dos campos preenchidos

        Quando submeto o seguinte formulário de acesso:
            | email         | senha         |
            | <email_input> | <senha_input> |
        Então o botão de entrar desabilita

        Exemplos:
            | email_input                  | senha_input |
            |                              | Comum190@   |
            | sobawi7447@u461.com          |             |


    @alerta_comum
    Esquema do Cenario: Tentativa de Login inválido com valores incorretos

        Quando submeto o seguinte formulário de acesso:
            | email         | senha         |
            | <email_input> | <senha_input> |
            E clico no botão entrar
        Então vejo a mensagem de alerta: "<mensagem_output>"

        Exemplos:
            | email_input                  | senha_input | mensagem_output                                        |
            | vany.qateste@teste.com       | Comum190@   | Usuário não encontrado                                 |
            | sobawi7447@u461.com          | Teste10     | A senha está incorreta ou não possui um formato válido |
