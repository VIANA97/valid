#language: pt

Funcionalidade: Acesso Admin
    Sendo um usuário do tipo Admin
    Quero fazer o login
    Para que eu possa acessar o contexto que estou atrelado

    Contexto:
        Dado que acesso a página de login

    @login_admin
    Cenario: Fazer o login com sucesso

        Quando submeto o seguinte acesso "vanessaviana30@gmail.com" e "Admin190@"
            E clico no botão entrar
        Então sou redirecionado para o meu contexto


    @botao_admin
    Esquema do Cenario: Tentativa de Login inválido sem um dos campos preenchidos

        Quando submeto o seguinte formulário de acesso:
            | email         | senha         |
            | <email_input> | <senha_input> |
        Então o botão de entrar desabilita

        Exemplos:
            | email_input                  | senha_input |
            |                              | Admin190@   |
            | vanessaviana30@gmail.com     |             |


    @alerta_admin
    Esquema do Cenario: Tentativa de Login inválido com valores incorretos

        Quando submeto o seguinte formulário de acesso:
            | email         | senha         |
            | <email_input> | <senha_input> |
            E clico no botão entrar
        Então vejo a mensagem de alerta: "<mensagem_output>"

        Exemplos:
            | email_input                  | senha_input | mensagem_output                                        |
            | vany.qateste@teste.com       | Admin190@   | Usuário não encontrado                                 |
            | vanessaviana30@gmail.com     | Teste10     | A senha está incorreta ou não possui um formato válido |
