Funcionalidade: Pre Condições
        Sendo um usuário do tipo VALID
        Quero fazer o login
        Para que eu possa acessar o painel VALID
        Tentar cadastrar uma nova empresa com dados inválidos
        Tentar cadastrar um usuário com dados inválidos
        Tentar cadastrar dentro de cada contexto uma marca, perfil e usuários com dados inválidos

        Contexto:
                #O acesso pode ser alterado para outro acesso tipo VALID existente no Customer Portal
                Dado que acesso a página de login
                        E submeto o seguinte acesso VALID "dev-valid@modalgr.com.br" e "123456" 
                        E seleciono a linguagem para português

        @customerportal
        Cenario: Tentando cadastrar uma nova empresa

        Cenario: Tentando cadastrar um novo usuário

        @dlv
        Cenario: Fluxos alternativos de cadastro dentro do Delivery Center

        @edi
        Cenario: Fluxos alternativos de cadastro dentro do E-diploma

        @blux
        Cenario: Fluxos alternativos de cadastro dentro do Blu X
