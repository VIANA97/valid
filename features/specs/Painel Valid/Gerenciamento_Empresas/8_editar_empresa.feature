#language: pt

Funcionalidade: Editar Empresas
    Sendo um usuário do tipo VALID
    Quero ter acesso a tela Gerenciamento de Empresas
    Para realizar a edição dos dados das empresas

    Contexto:
        Dado que acesso a página de login
            E submeto o seguinte acesso VALID "dev-valid@modalgr.com.br" e "123456"
            E estou na tela de Gerenciar Empresa

    #####################Editar Empresa#########################################
    @edicao
    Esquema do Cenario: Usuário VALID realiza a edição válida de uma empresa.

            E realizo a pesquisa da empresa "Eraser"
        Quando seleciono uma empresa para edicao
            E submeto a alteracao dos campos:
            | nome         | email         | chave         | exp_senha    |
            | <nome_input> | <email_input> | <chave_input> | <data_input> |
            E clico no botão salvar
        Então vejo a mensagem de alerta: "Empresa atualizada com sucesso!"

        Exemplos:

            | nome_input | email_input         | chave_input | data_input |
            |            | nerds@company.com   | ADDRESS     | 10         |
            |            |                     | EMAIL       | 28         |
            |            | blue@company.com    | PHONE       |            |
            |            | raccer@company.com  |             | 20         |
            | Edition    | edition@company.com | ADRESS      | 30         |

    @edit_inv1
    Esquema do Cenario: Usuário VALID realiza a edição inválida de uma empresa.

            E realizo a pesquisa da empresa "Edition"
        Quando seleciono uma empresa para edicao
            E submeto a alteracao dos campos invalidando-os:
            | nome         | email         | chave         | exp_senha    |
            | <nome_input> | <email_input> | <chave_input> | <data_input> |
        Então botão salvar desabilita

        Exemplos:

            | nome_input | email_input        | chave_input | data_input |
            |            | nerds@company.com  | ADDRESS     | 10         |
            | Edition    |                    | EMAIL       | 28         |
            | Edition    | blue@company.com   | PHONE       |            |
            | Edition    | raccer@company.com |             | 20         |

    @edit_inv2
    Esquema do Cenario: Usuário VALID realiza a edição inválida de uma empresa.

            E realizo a pesquisa da empresa "Edition"
        Quando seleciono uma empresa para edicao
            E submeto a alteracao dos campos invalidando-os:
            | nome     | email                 | chave   | exp_senha |
            | ValidPay | validpay@validpay.com | ADDRESS | 30        |
            E clico no botão salvar
        Então vejo a mensagem de alerta: "Ocorreu um erro interno ao processar seu pedido, por favor tente novamente!"