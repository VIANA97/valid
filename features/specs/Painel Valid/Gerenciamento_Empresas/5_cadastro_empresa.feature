#language: pt

Funcionalidade: Gerenciamento de Empresas
    Sendo um usuário do tipo VALID
    Quero ter acesso a tela Gerenciamento de Empresas
    Para realizar o cadastramento dos dados das empresas

    Contexto:
        Dado que acesso a página de login
            E submeto o seguinte acesso VALID "vanessa.souza@modalgr.com.br" e "Valid180@"
            E estou na tela de Gerenciar Empresa
        Quando clico no botão cadastrar nova empresa

    #####################Cadastrar Empresa#########################################
    @cadastro
    Esquema do Cenario: Usuário VALID realiza o cadastro de uma empresa.

            E submeto o preenchimento do primeiro passo:
            | nome         | email         | chave         | exp_senha    |
            | <nome_input> | <email_input> | <chave_input> | <data_input> |
            E submeto o preenchimento do segundo passo:
            | dlv         | edi         | blx         |
            | <produto_1> | <produto_2> | <produto_3> |
            E submeto o preenchimento do terceiro passo:
            | cor_pri      | cor_sec      | logo         |
            | <cor1_input> | <cor2_input> | <logo_input> |
        Então vejo a mensagem de alerta: "Empresa criada com successo!"

        Exemplos:

            | nome_input    | email_input        | chave_input | data_input | produto_1       | produto_2 | produto_3 | cor1_input | cor2_input | logo_input |
            | Nerds Company | nerds@company.com  | ADDRESS     | 20         | Delivery Center | E-diploma | Blu X     | #ff2813    | #000000    | nerds.jpg  |
            | Choam         | choam@company.com  | EMAIL       | 20         |                 | E-diploma |           | #ffc913    | #ff2813    | choam.jpg  |
            | Blue Ltda.    | blue@company.com   | PHONE       | 20         | Delivery Center |           |           | #2e13ff    | #187467    | blue.jpg   |
            | Raccer        | raccer@company.com | ADRESS      | 20         |                 |           | Blu X     | #1c1264    | #49705c    | raccer.jpg |
            | Eraser        | eraser@company.com | ADRESS      | 20         |                 |           | Blu X     | #e20000    | #000000    |            |


    #####################Invalidado Cadastrar Empresa#########################################
    @invalido1
    Esquema do Cenario: Usuário VALID tenta avançar para o passo 2 preenchendo campos incorretamente

            E submeto o preenchimento inválido do primeiro passo:
            | nome         | email         | chave         | exp_senha    |
            | <nome_input> | <email_input> | <chave_input> | <data_input> |
        Então botão próximo desabilita

        Exemplos:

            | nome_input | email_input        | chave_input | data_input |
            |            | nerds@company.com  | ADDRESS     | 20         |
            | Choam      |                    | EMAIL       | 29         |
            | Blue Ltda. | blue@company.com   |             | 19         |
            | Raccer     | raccer@company.com | ADRESS      |            |
            | Eraser     | eraser@company.com | ADRESS      | 50         |
            | Clear      | clear.company.com  | EMAIL       | 20         |
            
    @invalido2
    Cenario: Usuário VALID tenta fazer cadastro de uma empresa com e-mail já cadastrado

            E submeto o preenchimento do primeiro passo:
            | nome     | email                 | chave   | exp_senha |
            | ValidPay | validpay@validpay.com | ADDRESS | 30        |
            E seleciono um produto do segundo passo
            E submeto o preenchimento do terceiro passo:
            | cor_pri | cor_sec | logo         |
            | #0588c1 | #3c3d3d | validpay.png |
        Então vejo a mensagem de alerta: "Está empresa já está registrada para o e-mail"
