#language: pt

Funcionalidade: Ativar/Desativar Empresas
    Sendo um usuário do tipo VALID
    Quero ter acesso a tela Gerenciamento de Empresas
    Para realizar a edição dos dados das empresas

    Contexto:
        Dado que acesso a página de login
            E submeto o seguinte acesso VALID "dev-valid@modalgr.com.br" e "123456"
            E estou na tela de Gerenciar Empresa

    #####################Editar Empresa#########################################
    @inativo
    Esquema do Cenario: Usuário VALID desativa empresas.

            E realizo a pesquisa da empresa:
            | empresa      |
            | <nome_input> |
        Quando seleciono uma empresa para edicao
            E inativo o status da empresa
            E clico no botão salvar
        Então vejo a mensagem de alerta: "Empresa atualizada com sucesso!"

        Exemplos:

            | nome_input |
            | Choam      |
            | Blue Ltda. |

    @ativo
    Esquema do Cenario: Usuário VALID desativa empresas.

            E realizo a pesquisa da empresa:
            | empresa      |
            | <nome_input> |
        Quando seleciono uma empresa para edicao
            E ativo o status da empresa
            E clico no botão salvar
        Então vejo a mensagem de alerta: "Empresa atualizada com sucesso!"

        Exemplos:

            | nome_input |
            | Choam      |
            | Blue Ltda. |
