#language: pt

Funcionalidade: Gerenciamento de Empresas
    Sendo um usuário do tipo VALID
    Quero ter acesso a tela Gerenciamento de Empresas
    Para que a consulta dos dados das empresas

    Contexto:
        Dado que acesso a página de login
            E submeto o seguinte acesso VALID "dev-valid@modalgr.com.br" e "123456"
    #####################Gerenciamento de Empresas#########################################

    Cenario: Usuário VALID acessa a tela de Gerenciamento de Empresas

        Quando clico no opção Gerenciamento de Empresas
        Então sou redirecionado a página Gerenciar Empresa

    #####################Consulta/Visualização de Empresa##################################
    @consulta
    Esquema do Cenario: Usuário VALID realiza a consulta utilizando uma empresa cadastrada no sistema.

            E estou na tela de Gerenciar Empresa
        Quando submeto a seguinte pesquisa valida:
            | nome         | email         |
            | <nome_input> | <email_input> |
        Então sistema retornará empresa dos campos pesquisado

        Exemplos:

            | nome_input | email_input      |
            | Blue Ltda. |                  |
            |            | blue@company.com |

    @consulta_inv
    Esquema do Cenario: Usuário VALID tenta realizar consulta utilizando uma empresa não cadastrado no sistema.

            E estou na tela de Gerenciar Empresa
        Quando submeto a seguinte pesquisa invalida:
            | nome         | email         |
            | <nome_input> | <email_input> |
        Então vejo mensagem de alerta: "Não há informações a serem listadas"

        Exemplos:

            | nome_input | email_input                 |
            | Nao Existe |                             |
            |            | naoexiste_empresa@teste.com |