#language: pt

Funcionalidade: Pre Condições
        Sendo um usuário do tipo VALID
        Quero fazer o login
        Para que eu possa acessar o painel VALID
        Cadastrar uma nova empresa, habilitando todos os produtos
        Acessar essa empresa e os seus contextos
        Cadastrar dentro de cada contexto uma marca, perfil e usuários

        Contexto:
                #O acesso pode ser alterado para outro acesso tipo VALID existente no Customer Portal
                Dado que acesso a página de login
                        E submeto o seguinte acesso VALID "dev-valid@modalgr.com.br" e "123456" 
                        E seleciono a linguagem para português

        @customerportal
        Cenario: Cadastrando nova empresa
                        E clico na tela de "Gerenciamento de Empresas"
                        E clico no botão cadastrar nova empresa
                        E submeto o preenchimento do primeiro passo:
                        | nome              | email                 | chave | exp_senha |
                        | Quality Assurance | qa@testes.com         | PHONE | 30        |
                        E submeto o preenchimento do segundo passo:
                        | dlv | edi | blx |
                        | 1   | 2   | 3   |
                        E submeto o preenchimento do terceiro passo:
                        #As cores utilizadas podem ser alteradas para cores de sua preferência
                        | cor_pri | cor_sec | logo          |
                        | #ff2813 | #000000 |               | 
                        E vejo a mensagem de alerta: "Empresa criada com successo!"
                        E clico no para fechar a mensagem de sucesso
                        E submeto a seguinte pesquisa valida:
                        | nome              | email |
                        | Quality Assurance |       |
                        E acesso a empresa "Quality Assurance"
                        E acesso a tela de "Configuração da API"
                        E clico para realizar a configuração da api:
                        #Para cada empresa cadastrada, será necessário gerar outro número de CNPJ. 
                        #Caso isso não ocorra, poderá apresentar erros de integração com a API da Blu X
                        | CNPJ               | ID            | Regras        |
                        | 77.435.516/0001-93 | qa_teste_255 | Administrador |
                        E clico no para fechar a mensagem de sucesso
                        E acesso a tela de "Faturamento"
                        E clico na "Faixa de Faturamentos"
                        E acesso a tela de cadastro da tarifa "PIX"
                        E clico para cadastrar tarifa pix
                        E clico no para fechar a mensagem de sucesso
                        E acesso a tela de cadastro da tarifa "Boleto de cobrança"
                        E clico para cadastrar tarifa boleto
                        E clico no para fechar a mensagem de sucesso
                        #E acesso a tela de cadastro da tarifa "E-mail"
                        # E clico para cadastrar tarifa boleto
                        # E clico no para fechar a mensagem de sucesso
                        #E acesso a tela de cadastro da tarifa "SMS"
                        # E clico para cadastrar tarifa boleto
                        # E clico no para fechar a mensagem de sucesso
                        #E acesso a tela de cadastro da tarifa "Impresso"
                        # E clico para cadastrar tarifa boleto
                        # E clico no para fechar a mensagem de sucesso
                        #E acesso a tela de cadastro da tarifa "RCS"
                        # E clico para cadastrar tarifa boleto
                        # E clico no para fechar a mensagem de sucesso
                        #E acesso a tela de cadastro da tarifa "Whatsapp"
                        # E clico para cadastrar tarifa boleto
                        # E clico no para fechar a mensagem de sucesso
                        #E acesso a tela de cadastro da tarifa "Assinaturas"
                        # E clico para cadastrar tarifa boleto
                        # E clico no para fechar a mensagem de sucesso

        Cenario: Cadastrando novo usuário
                        # E clico na tela de "Gerenciar Usuários"
                        # E clico para cadastrar um usuário Valid
                        # E clico para cadastrar um usuário Admin de produto

        @dlv
        Cenario: Fluxos principais de cadastro dentro do Delivery Center
                        E clico na tela de "Acessar Empresa"
                        E acesso a empresa cadastrada no contexto "Delivery Center"
                        E clico na tela de "Gerenciar Administração"
                        E acesso a tela de Gerenciar Perfil
                        E clico para cadastrar um perfil dentro do produto dlv
                        E acesso a tela de Gerenciar Marcas
                        E clico para cadastrar uma marca dentro do produto dlv
                        E acesso a tela de Gerenciar Usuários
                        E clico para cadastrar um usuário Admin dentro do produto dlv
                        E clico para cadastrar um usuário Comum dentro do produto dlv
                        E acesso a tela de Configurar Canais
                        E clico para configurar canal de "SMS"
                        E clico no para fechar a mensagem de sucesso
                        E clico para configurar canal de E-mail
                        E clico no para fechar a mensagem de sucesso
                        E clico para configurar canal de "RCS"
                        E clico no para fechar a mensagem de sucesso
                        E clico para configurar canal de "Whatsapp"
                        E clico no para fechar a mensagem de sucesso
                        E retorno para o painel VALID
                        E clico na tela de "Gerenciamento de Empresas"
                        E submeto a seguinte pesquisa valida:
                        | nome              | email |
                        | Quality Assurance |       |
                        E acesso a empresa "Quality Assurance"
                        E acesso a tela de "Canais"
                        E clico para ativar canal de "SMS"
                        E clico para ativar canal de E-mail
                        E clico para ativar canal de "RCS"
                        E clico para ativar canal de "Whatsapp"
                        E clico para salvar
                        E clico no para fechar a mensagem de sucesso
                        E clico no botão inicio
                        E clico na tela de "Acessar Empresa"
                        E acesso a empresa cadastrada no contexto "Delivery Center"
                        E clico na tela de "Gerenciar Comunicação"
                        E clico no para fechar a mensagem de sucesso
                        E clico para bloquear canal de E-mail
                        E clico no para fechar a mensagem de sucesso
                        E clico para bloquear canal de Impresso
                        E clico no para fechar a mensagem de sucesso
                        E clico para bloquear canal de "RCS"
                        E clico no para fechar a mensagem de sucesso
                        E clico para bloquear canal de "SMS"
                        E clico no para fechar a mensagem de sucesso
                        E clico para bloquear canal de "Whatsapp"
                        E clico no para fechar a mensagem de sucesso
                        # E clico para desbloquear o canal de E-mail
                        # E clico para desbloquear o canal de Impresso
                        # E clico para desbloquear o canal de RCS
                        # E clico para desbloquear o canal de SMS
                        # E clico para desbloquear o canal de Whatsapp
                        # E acesso a tela de "Gerenciar Templates"
                        # E clico para cadastrar um template de SMS
                        # E clico para cadastrar um template de E-mail
                        # E clico para cadastrar um template de Impresso
                        # E submeto a seguinte pesquisa valida:
                        # | Procurar Template |
                        # | QA_TP_SMS_001     |
                        # E clico para finalizar o template de SMS
                        # E acesso a tela de "Jornadas"
                        # E clico para cadastrar uma Jornada
                        # E submeto a seguinte pesquisa valida:
                        # | Nome              |
                        # | QA_Jornada_001    |
                        # E clico para finalizar a Jornada
                        # E acesso a tela de "Campanhas"
                        # E clico para cadastrar uma Campanha por Template
                        # E clico para cadastrar uma Campanha por Jornada
                        # E submeto a seguinte pesquisa valida:
                        # | Nome              |
                        # | QA_Campanha_TP_001|
                        # E clico para finalizar a Campanha


        @edi
        Cenario: Fluxos principais de cadastro dentro do E-diploma
                        E clico na tela de "Acessar Empresa"
                        E acesso a empresa cadastrada no contexto "E-diploma"
                        E clico na tela de "Gerenciar Administração"
                        E acesso a tela de Gerenciar Perfil
                        E clico para cadastrar um perfil dentro do produto edi
                        E acesso a tela de Gerenciar Marcas
                        E clico para cadastrar uma marca dentro do produto edi
                        E acesso a tela de Gerenciar Usuários
                        E clico para cadastrar um usuário Admin dentro do produto edi
                        E clico para cadastrar um usuário Comum dentro do produto edi

        @blux
        Cenario: Fluxos principais de cadastro dentro do Blu X
                        E clico na tela de "Acessar Empresa"
                        E acesso a empresa cadastrada no contexto "Blu X"
                        E clico na tela de "Gerenciar Administração"
                        E acesso a tela de Gerenciar Perfil
                        E clico para cadastrar um perfil dentro do produto blux
                        E acesso a tela de Gerenciar Marcas
                        E clico para cadastrar uma marca dentro do produto blux
                        E acesso a tela de Gerenciar Usuários
                        E clico para cadastrar um usuário Comum dentro do produto blux




