require "capybara"
require "capybara/cucumber"
require "allure-cucumber"
require "webdrivers"

Capybara.configure do |config|
    config.default_driver = :selenium_chrome
    config.app_host = "http://dev-valid.mgr.services"
    config.default_max_wait_time = 5
end


AllureCucumber.configure do |config|
    config.results_directory = "/logs"
    config.clean_results_directory = true
end