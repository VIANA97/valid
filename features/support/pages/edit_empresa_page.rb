class Edicao 
    include Capybara::DSL

    def edicao_emp(edicao)
        find('input[id="name"]').set edicao[:nome] if edicao[:nome].length > 0
        find('input[id="email"]').set edicao[:email] if edicao[:email].length > 0
        find('input[id="keyCode"]').set edicao[:chave] if edicao[:chave].length > 0
        find('input[id="passwordExpire"]').set edicao[:exp_senha] if edicao[:exp_senha].length > 0
    end

    def edicao_inv1_emp(edicao_inv1)
        find('input[id="name"]').set edicao_inv1[:nome]
        find('input[id="email"]').set edicao_inv1[:email]
        find('input[id="keyCode"]').set edicao_inv1[:chave]
        find('input[id="passwordExpire"]').set edicao_inv1[:exp_senha] 
    end

    def botao_salvar
        click_button 'Salvar'
    end
end