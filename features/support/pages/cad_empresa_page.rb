class Cad_empresa
    include Capybara::DSL
    
    def botao_cadastrar
        sleep 2
        find('a', text: 'Cadastrar nova empresa').click
    end

    def botao_proximo
        click_button 'Próximo'
    end

    def botao_finalizar
        click_button 'Finalizar'
    end
    

    def cad_empresa(cadastro) ##passo 1
        find('input[placeholder="Digite o nome da empresa"]').set cadastro[:nome]
        find('input[placeholder="emaildaempresa@email.com"]').set cadastro[:email]
        find('input[placeholder="Chave forte"]').set cadastro[:chave]
        find('input[placeholder="Expiração de senha"]').set cadastro[:exp_senha]
    end

    def escolhe_produto(produto) ## passo 2
        find('i[class="fa-2x fa-mail-bulk fas"]').click if produto[:dlv].length > 0
        find('i[class="fa-2x fa-university fas"]').click if produto[:edi].length > 0
        find('i[class="fa-2x fa-receipt fas"]').click if produto[:blx].length > 0
    end

    def customizacao(estilo) # passo 3
        find('label[for="color1"]').click
        find('div[class="hex-text"]').find('input').set estilo[:cor_pri]
        click_button 'Salvar'
    
        find('label[for="color2"]').click
        find('div[class="hex-text"]').find('input').set estilo[:cor_sec]
        click_button 'Salvar'

        caminho = Dir.pwd + "/features/support/fixtures/images/"
        page.driver.browser.find_element(:id, "fileDropRef").send_keys(caminho + estilo[:logo]) if estilo[:logo].length > 0
    end


end