class Pre_condicao

    include Capybara::DSL
 
    def seleciono_ling
        sleep 1
        find('.language').click
        sleep 1
        find('.language-list').find('img[src="assets/images/icons/pt.png"]').click
    end
    def fechar_mensagem
        sleep 1
        find('i[class*="fas fa-times"]').click
    end

    def acesso_emp(empresa)
        sleep 1
        find('td', text: empresa).click
        sleep 1
    end

    def acesso_api(api)
        find('h1', text: api).click
        sleep 1
    end

    def config_API(config_api)
        find('input[placeholder="00.000.000/0000-00"]').set config_api[:CNPJ]
        sleep 1
        find('input[placeholder=""]').set config_api[:ID]
        sleep 1
        find('input[class="default_selector"]').click
        sleep 1
        click_button config_api[:Regras]
        sleep 1
        click_button 'Salvar'
    end

    def acesso_ffaturamentos(f_faturamentos)
        find('span', text: f_faturamentos).click
        sleep 1
    end

    def acesso_cadPIX(pix)
        sleep 1
        find('input[class="default_selector"]').click
        sleep 1
        click_button pix
        sleep 1
    end

    def cad_faixa_blux
        sleep 2
        find('p', text: 'Nova faixa').click
        sleep 1
        find('input[placeholder="Valor desejado"]').set "2"
        sleep 1
        click_button 'Inserir'
        sleep 1
        click_button 'Salvar'
        sleep 1
    end

    def produto_contexto(produto)
        find('img[alt="Quality Assurance"]').click ##Alterar o nome da empresa no alt
        sleep 1
        find('span', text: produto).click
        sleep 2
    end

    def cad_perfil_dlv
        sleep 1
        find('a[class="button-primary link-width w-100 mt-4 mr-auto"]').click
        find('input[id="txt_name"]').set "Perfil_Automação_DLV"
        find('label', text: 'Área de Relatório').click
        click_button "Finalizar"
        sleep 1
        find('i[class*="fas fa-times"]').click
    end

    def cad_marca_dlv
        sleep 1
        find('a[class="button-primary link-width w-100"]').click
        sleep 1
        find('input[id="txt_name"]').set "Marca_Automação_DLV"
        sleep 1
        click_button "Finalizar"
        sleep 1
        find('i[class*="fas fa-times"]').click
    end

    def cad_user_admin
        sleep 1
        click_button "Cadastrar novo usuário"
        sleep 1
        find('input[placeholder="Nome do usuário"]').set "Maria - Usuário Admin"
        find('input[placeholder="valid@valid.com"]').set "maria@admin.com"
        find('input[placeholder="+00 (00) 00000-0000"]').set "5513988888888"
        find('label[for="admin_id"]').click
        sleep 1
        click_button "Cadastrar"
        sleep 1
        find('i[class*="fas fa-times"]').click
    end

    def cad_user_comum
        sleep 1
        click_button "Cadastrar novo usuário"
        sleep 1
        find('input[placeholder="Nome do usuário"]').set "João - Usuário Comum"
        find('input[placeholder="valid@valid.com"]').set "joao@comum.com"
        find('input[placeholder="+00 (00) 00000-0000"]').set "5513988888888"
        find('label[for="common_id"]').click
        find('app-select[ng-reflect-label="Selecione o perfil"]').find('div[class="dropdown-label"]').click
        click_button 'Perfil_Automação_DLV'
        sleep 1
        find('app-select-multi[ng-reflect-label="Selecione as marcas"]').find('div[class="dropdown-container"]').click
        sleep 1
        find('button', text: 'Marca_Automação_DLV').click
        find('app-select-multi[ng-reflect-label="Selecione as marcas"]').find('div[class="dropdown-container"]').click
        click_button "Cadastrar"
        find('i[class*="fas fa-times"]').click
    end

    def config_canal_num(numero)
        find('a', text: numero).click
        click_button "Novo cadastro"
        sleep 1
        find('mat-select[role="combobox"]').click
        sleep 1
        find('span', text: "Todos").click
        script = "document.getElementsByClassName('cdk-overlay-backdrop cdk-overlay-transparent-backdrop cdk-overlay-backdrop-showing')[0].click();"
        page.execute_script(script)
        sleep 1
        click_button "Inserir"
        sleep 1
        click_button "Salvar"
        sleep 1
    end

    def config_canal_email
        find('a', text: "E-mail").click
        sleep 1
        click_button "Novo cadastro"
        sleep 1
        find('mat-select[role="combobox"]').click
        sleep 1
        find('span', text: "Todos").click
        script = "document.getElementsByClassName('cdk-overlay-backdrop cdk-overlay-transparent-backdrop cdk-overlay-backdrop-showing')[0].click();"
        page.execute_script(script)
        sleep 1
        find('input[placeholder="@valid.com"]').set "@companyteste.com"
        sleep 1
        find('span', text: "Adquirir um novo domínio").click
        sleep 1
        click_button "Inserir"
        sleep 1
        click_button "Salvar"
        sleep 1
    end

    def ativando_canal_num
        find('i[class*="fa-hover"]').click
        sleep 1
        first('input[class*="ng-star-inserted"]').set "5324"
        sleep 1
        find('div[class="mat-slide-toggle-thumb"]').click
        sleep 1
        find('app-input[label="USER"]').find('input[class*="ng-star-inserted"]').set "Agnaldo" ##mudar nome
        sleep 1
        find('app-input[label="PASSWORD"]').find('input[class*="ng-star-inserted"]').set "QA2021"
        sleep 1
        click_button "Inserir"
        sleep 1
    end
    def ativando_canal_email
        sleep 1
        find('i[class*="fa-hover"]').click
        sleep 1
        find('input[placeholder="@valid.com"]').set "@company.teste.br"
        sleep 1
        find('div[class="mat-slide-toggle-thumb"]').click
        sleep 1
        find('app-input[label="USER"]').find('input[class*="ng-star-inserted"]').set "Agnaldo" ##mudar nome
        sleep 1
        find('app-input[label="PASSWORD"]').find('input[class*="ng-star-inserted"]').set "QA2021"
        sleep 1
        click_button "Inserir"
        sleep 1
    end
    def botao_inicio
        elemento = find('app-internal-nav-bar')
        elemento.first('li').click
    end

    def esc
        script = "document.getElementsByClassName('cdk-overlay-backdrop cdk-overlay-transparent-backdrop cdk-overlay-backdrop-showing')[0].click();"
        page.execute_script(script)
    end

    def canal_email_bloq
        find('input[placeholder="valid@valid.com"]').set "teste1@teste.com"
        combo = all('mat-select[role="combobox"]').last
        combo.click
        find('span', text: "Todos").click
        script = "document.getElementsByClassName('cdk-overlay-backdrop cdk-overlay-transparent-backdrop cdk-overlay-backdrop-showing')[0].click();"
        page.execute_script(script)
        click_button "Inserir"
        sleep 1
    end

    def canal_impresso_bloq
        find('input[ng-reflect-mask-expression="00000000"]').set "11013923"
        sleep 1
        #find('input[class="ng-pristine ng-valid ng-star-inserted ng-touched"]').set "2"
        combo = all('mat-select[role="combobox"]').last
        combo.click
        find('span', text: "Todos").click
        script = "document.getElementsByClassName('cdk-overlay-backdrop cdk-overlay-transparent-backdrop cdk-overlay-backdrop-showing')[0].click();"
        page.execute_script(script)
        click_button "Inserir"
        sleep 1
    end

    def canal_num_bloq
        find('input[placeholder="+00 (00) 00000-00000"]').set "55134101-0010"
        combo = all('mat-select[role="combobox"]').last
        combo.click
        find('span', text: "Todos").click
        script = "document.getElementsByClassName('cdk-overlay-backdrop cdk-overlay-transparent-backdrop cdk-overlay-backdrop-showing')[0].click();"
        page.execute_script(script)
        click_button "Inserir"
        sleep 1
    end

    def cad_perfil_edi
        sleep 1
        find('a[class*="button-primary"]').click
        sleep 1
        find('input[id="txt_name"]').set "Perfil_Automação_EDI"
        sleep 1
        first('div[class="form-check"]').click
        sleep 1
        click_button "Finalizar"
        sleep 1
        find('i[class*="fas fa-times"]').click
    end

    def cad_marca_edi
        sleep 1
        find('a[class*="button-primary"]').click
        sleep 1
        find('input[id="txt_name"]').set "Marca_Automação_EDI"
        sleep 1
        click_button "Finalizar"
        sleep 1
        find('i[class*="fas fa-times"]').click
    end

    def cad_user_admin_edi
        sleep 1
        click_button "Cadastrar novo usuário"
        sleep 1
        find('input[placeholder="Nome do usuário"]').set "Fernanda - Usuário Admin"
        sleep 1
        find('input[placeholder="valid@valid.com"]').set "fernanda@admin.com"
        sleep 1
        find('input[placeholder="+00 (00) 00000-0000"]').set "5513988887778"
        sleep 1
        find('label[for="admin_id"]').click
        sleep 1
        click_button "Cadastrar"
        sleep 1
        find('i[class*="fas fa-times"]').click
    end

    def cad_user_comum_edi
        sleep 1
        click_button "Cadastrar novo usuário"
        sleep 1
        find('input[placeholder="Nome do usuário"]').set "Ricardo - Usuário Comum"
        sleep 1
        find('input[placeholder="valid@valid.com"]').set "ricardo@comum.com"
        sleep 1
        find('input[placeholder="+00 (00) 00000-0000"]').set "5513988888888"
        sleep 1
        find('label[for="common_id"]').click
        sleep 1
        find('app-select[ng-reflect-label="Selecione o perfil"]').find('div[class="dropdown-label"]').click
        sleep 1
        click_button 'Perfil_Automação_EDI'
        sleep 1
        find('app-select-multi[ng-reflect-label="Selecione as marcas"]').find('div[class="dropdown-container"]').click
        sleep 1
        find('button', text: 'Marca_Automação_EDI').click
        find('app-select-multi[ng-reflect-label="Selecione as marcas"]').find('div[class="dropdown-container"]').click
        click_button "Cadastrar"
        find('i[class*="fas fa-times"]').click
    end

    def cad_perfil_blux
        sleep 1
        find('a[class*="button-primary"]').click
        sleep 1
        find('input[id="txt_name"]').set "Perfil_Automação_BLUX"
        sleep 1
        find('label', text: 'Parceiros').click
        sleep 1
        click_button "Finalizar"
        sleep 1
        find('i[class*="fas fa-times"]').click
    end

    def cad_marca_blux
        sleep 1
        find('a[class*="button-primary"]').click
        sleep 1
        find('input[id="txt_name"]').set "Marca_Automação_BLUX"
        sleep 1
        click_button "Finalizar"
        sleep 1
        find('i[class*="fas fa-times"]').click
    end

    def cad_user_comum_blux
        sleep 1
        click_button "Cadastrar novo usuário"
        sleep 1
        find('input[placeholder="Nome do usuário"]').set "Rodrigo - Usuário Comum"
        sleep 1
        find('input[placeholder="valid@valid.com"]').set "rodrigo@comum.com"
        sleep 1
        find('input[placeholder="+00 (00) 00000-0000"]').set "5513988888888"
        sleep 1
        find('label[for="common_id"]').click
        sleep 1
        find('app-select[ng-reflect-label="Selecione o perfil"]').find('div[class="dropdown-label"]').click
        sleep 1
        click_button 'Perfil_Automação_BLUX'
        sleep 1
        find('app-select-multi[ng-reflect-label="Selecione as marcas"]').find('div[class="dropdown-container"]').click
        sleep 1
        find('button', text: 'Marca_Automação_BLUX').click
        find('app-select-multi[ng-reflect-label="Selecione as marcas"]').find('div[class="dropdown-container"]').click
        click_button "Cadastrar"
        find('i[class*="fas fa-times"]').click
    end
end