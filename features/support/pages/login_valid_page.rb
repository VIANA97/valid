class LoginPage
    include Capybara::DSL


    def with(email, senha)
        find("#txt_login").set email
        find("#txt_password").set senha
    end

    def botao_entrar
        click_button "Entrar"
    end
end