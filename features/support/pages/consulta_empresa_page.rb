class Consulta_emp
    include Capybara::DSL
    def campo_nome
        sleep 2
        elemento = find('app-input[formcontrolname="name"]')
        elemento.first('input')
    end

    def campo_email
        sleep 2
        elemento = find('app-input[ng-reflect-label="E-mail"]')
        elemento.first('input')
    end 
    
    def botao_pesquisar
        sleep 2
        find('div[class^="w-100"]').click
    end

end