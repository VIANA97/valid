Before do
    @login_valid = LoginPage.new
    @ger_empresa = GerenciamentoEmpresa.new
    @cad_empresa =  Cad_empresa.new
    @consulta_emp = Consulta_emp.new
    @edit_empresa = Edicao.new
    @pre_condicao = Pre_condicao.new

    page.driver.browser.manage.window.maximize
end

After do
    temp_shot =  page.save_screenshot("logs/temp_screenshot.png")

    Allure.add_attachment(
        name: "Screenshot",
        type: Allure::ContentType::PNG,
        source: File.open(temp_shot),
   )
end